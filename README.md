# REDUCE
Project Link: https://gitlab.oit.duke.edu/wl227/reduce

## System specification
CentOS 7 x86_64

## Download required files
Download the rpm file of reduce from https://sourceforge.net/projects/reduce-algebra/.
An older version is choose to avoid using packages from CentOS 8. The attached rpm file of reduce is from the snapshot_2022-10-07, which can also be downloaded through the following link:
- https://master.dl.sourceforge.net/project/reduce-algebra/snapshot_2020-10-07/linux64/reduce-complete-5424-1.x86_64.rpm?viasf=1

```
[root@electron REDUCE]# rpm -i reduce-complete-5424-1.x86_64.rpm 
error: Failed dependencies:
	libstdc++.so.6(CXXABI_1.3.8)(64bit) is needed by reduce-complete-5424-1.x86_64
	libstdc++.so.6(CXXABI_1.3.9)(64bit) is needed by reduce-complete-5424-1.x86_64
	libstdc++.so.6(GLIBCXX_3.4.20)(64bit) is needed by reduce-complete-5424-1.x86_64
	libstdc++.so.6(GLIBCXX_3.4.21)(64bit) is needed by reduce-complete-5424-1.x86_64
```

Two other packages attached are needed to install the reduce. Which can also be downloaded using the following links:
- https://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64/cuda-nsight-systems-10-1-10.1.168-1.x86_64.rpm
- https://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64/cuda-license-10-1-10.1.168-1.x86_64.rpm


## Installation
1. Run the following commands:

```
[root@cipher REDUCE]# rpm -i cuda-license-10-1-10.1.168-1.x86_64.rpm 
warning: cuda-license-10-1-10.1.168-1.x86_64.rpm: Header V4 RSA/SHA512 Signature, key ID d42d0685: NOKEY
*** LICENSE AGREEMENT ***
By using this software you agree to fully comply with the terms and 
conditions of the EULA (End User License Agreement). The EULA is located
at /usr/local/cuda-10.1/doc/EULA.txt. The EULA can also be found at
http://docs.nvidia.com/cuda/eula/index.html. If you do not agree to the
terms and conditions of the EULA, do not use the software.
[root@cipher REDUCE]# rpm -i cuda-nsight-systems-10-1-10.1.168-1.x86_64.rpm 
warning: cuda-nsight-systems-10-1-10.1.168-1.x86_64.rpm: Header V4 RSA/SHA512 Signature, key ID d42d0685: NOKEY
[root@cipher REDUCE]# rpm -i reduce-complete-5424-1.x86_64.rpm 

```

2. Update the "libstdc++.so.6"
- Downloaded attached "libstdc++.so.6.0.22" file
- Put it in "/usr/lib64"
- Delete the original softlink of "libstdc++.so.6" 
- Create a new softlink of "libstdc++.so.6 -> libstdc++.so.6.0.22" .
```
[root@cipher lib64]# cd /usr/lib64
[root@cipher lib64]# ls -ltr libstd*
-rwxr-xr-x. 1 root root  830776 Mar  5  2015 libstdc++.so.5.0.7
lrwxrwxrwx. 1 root root      18 Mar 18  2020 libstdc++.so.5 -> libstdc++.so.5.0.7
-rwxr-xr-x  1 root root  995840 Sep 29  2020 libstdc++.so.6.0.19
lrwxrwxrwx  1 root root      19 Nov  2  2021 libstdc++.so.6 -> libstdc++.so.6.0.19
-rwxr-xr-x  1 root root 1561792 May 26 12:08 libstdc++.so.6.0.22
[root@cipher lib64]# rm libstdc++.so.6
rm: remove symbolic link ‘libstdc++.so.6’? y
[root@cipher lib64]# ln -s libstdc++.so.6.0.22 libstdc++.so.6

```

3. Install gnuplot
- Download "gnuplot-5.4.3.tar.gz" and unzip it
```
tar -xvf gnuplot-5.4.3.tar.gz ./
cd gnuplot-5.4.3
```
- configure, make and make install
```
./configure
make
make check
make install
``` 

## Test
1. Terminal test, using "redpsl" to start in Terminal. 

```
[root@electron lib64]# redpsl
Loading image file: /usr/lib/reduce/pslbuild/red/reduce.img 
Reduce (Free PSL version, revision 5424),  7-Oct-2020 ...

1: x^x;
 x
x
2: df(ws,x);
 x
x *(log(x) + 1)
3: int(ws,x);
 x
x
4: bye;
Quitting
[root@electron lib64]# 
```

2. GUI test, using "redcsl" to start in Terminal. 

![image](https://gitlab.oit.duke.edu/wl227/reduce/-/raw/main/Test/ReduceTest.png)


3. Test gnuplot
```
plot (cos sqrt(x**2 + y**2),x=(-3 .. 3),y=(-3 .. 3),hidden3d);
```

![image](https://gitlab.oit.duke.edu/wl227/reduce/-/raw/main/Test/ReduceTest_Gnuplot.png)


